import { Controller, Get, Post, Delete, Put, Query, Body, Param } from '@nestjs/common';
import { TodoService } from './todo.service';
import { TodoDto } from './dto/todo.dto';

@Controller('todo')
export class TodoController {
    constructor(private todoService: TodoService) {}

  @Get()
  public getCars() {
    return this.todoService.getTodo();
  }
  @Post()
  public postCar(@Body() todo: TodoDto) {
    return this.todoService.postTodo(todo);
  }
  @Get(':id')
  public async getCarById(@Param('id') id: number) {
    return this.todoService.getTodoById(id);
  }
  @Delete(':id')
  public async deleteTodoById(@Param('id') id: number) {
    return this.todoService.deleteTodoById(id);
  }
  @Put(':id')
  public async putTodoById(@Param('id') id: number, @Query() query) {
    const propertyName = query.property_name;
    const propertyValue = query.property_value;
    // const propertyDescription = query.property_description;
    // const propertyValueDescription = query.property_value_description;
    return this.todoService.putTodoById(id, propertyName, propertyValue);
  }
}
