import { Injectable, HttpException } from '@nestjs/common';
import { TODO } from './mock/todo.mock';

@Injectable()
export class TodoService {
    private todos = TODO;

  public getTodo() {
    return this.todos;
  }
  public postTodo(todo){
    this.todos.push(todo)
    return this.todos;
  }
  public getTodoById(id: number) :Promise<any>{
    const todoId = Number(id);
    return new Promise((resolve) => {
      const todo = this.todos.find((todo) => todo.id === todoId);
    if(todo){
      throw new HttpException('Not Found', 404);
    }
    return resolve(todo);
    } );
    
  }
  public deleteTodoById(id: number) :Promise<any>{
    const todoId = Number(id);
    return new Promise((resolve) => {
      const index = this.todos.findIndex((todo) => todo.id ===todoId);
    if(index === -1){
      throw new HttpException('Not Found', 404);
    }
    this.todos.splice(index, 1);
    return resolve(this.todos);
    } );
    
  }
  public putTodoById(id: number, propertyName: string, propertyValue: string) :Promise<any>{
    const todoId = Number(id);
    return new Promise((resolve) => {
      const index = this.todos.findIndex((todo) => todo.id === todoId);
    if(index === -1){
      throw new HttpException('Not Found', 404);
    }
    this.todos[index][propertyName] = propertyValue;
    
    return resolve(this.todos[index]);
    });
    
  }

}
